package projectCreator;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import org.eclipse.jgit.api.Git;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Darius on 31-Jul-18.
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public static class PackageRenamer extends Application {
        public static void main(String[] args) {
            launch(args);
        }

        @Override
        public void start(Stage primaryStage) throws Exception {
            primaryStage.setTitle("Libgdx project package renamer");
            VBox column = new VBox();
            column.setAlignment(Pos.CENTER);

            HBox line1 = new HBox(10);

            line1.getChildren().add(new Label("Project path"));
            line1.setAlignment(Pos.CENTER);
            line1.setPadding(new Insets(10));

            TextField pathField = new TextField(getUserHomeFolder());
            line1.getChildren().add(pathField);
            pathField.setMinWidth(360);

            Button browse = new Button("Browse...");
            browse.setOnAction(e -> {
                // Open file chooser
                DirectoryChooser chooser = new DirectoryChooser();
                chooser.setTitle("Choose project root folder");
                chooser.setInitialDirectory(new File(getUserHomeFolder()));

                File chosenFolder = chooser.showDialog(primaryStage);
                pathField.setText(chosenFolder.toString());
            });

            line1.getChildren().add(browse);

            column.getChildren().add(line1);

            HBox line2 = new HBox(10);
            line2.getChildren().add(new Label("Original package name:"));
            TextField origPack = new TextField("com.whatever.name");
            line2.getChildren().add(origPack);

            line2.getChildren().add(new Label("New package name:"));
            TextField newPack = new TextField("com.new.name");
            line2.getChildren().add(newPack);

            column.getChildren().add(line2);

            Button button = new Button("RENAME");
            column.getChildren().add(button);

            button.setOnMouseClicked(e -> {
                try {
                    replacePackageNameInProject(pathField.getText(), origPack.getText(), newPack.getText());

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("SUCCESS");
                    alert.setHeaderText("Packages renamed successfully");
                    alert.setContentText("");
                    alert.show();
                } catch (Exception exc) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("ERROR");
                    alert.setHeaderText("");
                    alert.setContentText(exc.getMessage());
                    alert.show();
                }
            });

            Scene scene = new Scene(column);
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setResizable(false);
        }
    }

    ComboBox<String> versionsList;
    TextField packageName;
    TextField firstVersion;
    TextField firstCommitMessage;

    @Override
    public void start(Stage primaryStage) {
        // Create all the UI
        primaryStage.setTitle("DareEngine Project Creator");

        VBox column = new VBox();

        HBox line1 = new HBox(10);

        line1.getChildren().add(new Label("Project destination"));
        line1.setAlignment(Pos.CENTER);
        line1.setPadding(new Insets(10));

        TextField pathField = new TextField(getUserHomeFolder());
        line1.getChildren().add(pathField);
        pathField.setMinWidth(360);

        Button browse = new Button("Browse...");
        browse.setOnAction(e -> {
            // Open file chooser
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Choose project destination folder");
            chooser.setInitialDirectory(new File(getUserHomeFolder()));

            File chosenFolder = chooser.showDialog(primaryStage);
            pathField.setText(chosenFolder.toString());
        });

        line1.getChildren().add(browse);

        column.getChildren().add(line1);

        HBox grids = new HBox(15);
        grids.setPadding(new Insets(10));
        GridPane grid1 = new GridPane();
        GridPane grid2 = new GridPane();
//        grid1.setPadding(new Insets(5));
//        grid2.setPadding(new Insets(5));

        grid1.setVgap(6);
        grid1.setHgap(5);
        grid2.setVgap(6);
        grid2.setHgap(5);

        grids.setAlignment(Pos.CENTER);

        grid2.add(new Label("Project name"), 0, 0);

        TextField name = new TextField("LibGdx_project");
        grid2.add(name, 1, 0);

        Label label = new Label("Package name");
        grid1.add(label, 0, 0);

        packageName = new TextField("com.libgdx.game");
        grid1.add(packageName, 1, 0);

        grid2.add(new Label("Initial commit message"), 0, 1);

        firstCommitMessage = new TextField("Start from template");
        grid2.add(firstCommitMessage, 1, 1);

        grid1.add(new Label("Initial tag version"), 0, 1);

        firstVersion = new TextField("0.1");
        grid1.add(firstVersion, 1, 1);

        grid1.setPadding(new Insets(0, 0, 0, 20));

        grids.getChildren().addAll(grid2, grid1);

        column.getChildren().add(grids);

        // version list
        HBox versionsTitle = new HBox(5);
        versionsTitle.setAlignment(Pos.CENTER);
        versionsTitle.setPadding(new Insets(20, 0, 0, 0));
        versionsTitle.getChildren().add(new Label("Template & DareEngine version"));

        Button updateInfo = new Button("Update Info");
        updateInfo.setOnMouseClicked(e -> {
            getGitInfo();
        });

        versionsTitle.getChildren().add(updateInfo);


        HBox versionsChooser = new HBox(10);
        versionsChooser.setAlignment(Pos.CENTER);

        versionsList = new ComboBox<>();
        versionsList.getItems().addAll();

        versionsChooser.getChildren().add(versionsList);

        column.getChildren().add(versionsTitle);
        column.getChildren().add(versionsChooser);

        HBox finalLine = new HBox();
        Button create = new Button("CREATE PROJECT");
        finalLine.setAlignment(Pos.CENTER);
        finalLine.setSpacing(10);
        finalLine.setPadding(new Insets(10));

        create.setOnMouseClicked(e -> {
            // CREATE PROJECT!

            // first look if the supplied package name is legal: only supported format is word.word.word
            Matcher matcher = getMatcherPackageName(packageName.getText().trim());

            if (!matcher.find() || !matcher.group(0).equals(packageName.getText())) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("Package name error");
                alert.setContentText("Specified package name is invalid. It must have the following format: word.word.word");
                alert.show();

                return;
            }

            // project folder path
            String path = pathField.getText() + File.separator + name.getText();

            try {
                // cloning the template project
                Git git = Git.cloneRepository().setURI("git@gitlab.com:dcostap/dareEngine-template.git")
                        .setDirectory(new File(path)).call();

                // checking to the tag version and moving to a new branch
                String templateVersion = "";
                Matcher matcher1 = Pattern.compile(".+?(?=')'(.+?(?='))").matcher(versionsList.getValue());
                matcher1.find();
                templateVersion = matcher1.group(1);
                git.checkout().addPath("tags/" + templateVersion).setCreateBranch(true).setName("newMaster").call();

                git.close();

                // delete the whole git repository, so it is only left the version we checked out before
                deleteDirectoryRecursively(path + File.separator + ".git");

                // create brand new git repository
                // replace all the package names that are by default in the template project with the ones supplied by the user
                replacePackageNameInProject(path, "com.libgdx.game", packageName.getText().trim());

                git = Git.init().setDirectory(new File(path)).call();
                git.add().addFilepattern(".").call();
                // first commit and tag
                git.commit().setMessage(firstCommitMessage.getText()).call();
                git.tag().setMessage(firstCommitMessage.getText()).setAnnotated(true).setName(firstVersion.getText()).call();

//                Files.delete(Paths.get(path + "/.git/index"));
//                git.reset().call();

                git.close();

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("SUCCESS");
                alert.setHeaderText("Successful Project Creation");
                alert.setContentText("Template project was created in: " + path + "\n\n");
                alert.show();
            } catch (Exception exc) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("Project creation error");
                alert.setContentText("There was an error cloning the repository from the gitlab page of the template project:" +
                        "\n\n" + exc);
                alert.show();
            }
        });

        finalLine.getChildren().add(create);

        column.getChildren().add(finalLine);

        Scene scene = new Scene(column);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setResizable(false);

        getGitInfo();
    }

    private static String getUserHomeFolder() {
        return System.getProperty("user.home");
    }

    private static Matcher getMatcherPackageName(String finalPackageName) {
        // matches only word.word.word. Each word is in a group
        return Pattern.compile("([a-zA-Z]+?(?=\\.))\\.([a-zA-Z]+?(?=\\.))\\.([a-zA-Z]*)").matcher(finalPackageName);
    }

    private boolean cancelAction = false;

    private void getGitInfo() {
        Alert loading = new Alert(Alert.AlertType.INFORMATION, "", ButtonType.CANCEL);
        loading.setOnCloseRequest(e -> {
            cancelAction = true;
        });
        loading.setTitle("Loading...");
        loading.setHeaderText("Loading Info from gitlab...");
        loading.show();

        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        StringBuilder webPage = new StringBuilder();

        // scrap the tag info from the gitlab page
        String tagWebPage = "https://gitlab.com/dcostap/dareEngine-template/tags";

        try {
            url = new URL(tagWebPage);
            is = url.openStream();  // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                webPage.append(line);
            }
        } catch (Exception e) {
            loading.close();
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("Connection error");
            alert.setContentText("There was an error retrieving the information from the gitlab page of the template project." +
                    "\nCheck your internet connection and try again");
            alert.show();
            return;
        }

        // pattern that scraps the string corresponding to the tag versions of the template project
        Pattern pattern = Pattern.compile("<a class=\\\"item-title ref-name prepend-left-4\\\" href=\\\"\\/dcostap\\/dareEngine-template\\/tags\\/.+?(?=>)>(.+?(?=<\\/a>))");
        Matcher matcher = pattern.matcher(webPage);

        int from = 0;

        ArrayList<String> versions = new ArrayList<>();
        ArrayList<String> templateVersions = new ArrayList<>();
        templateVersions.add("master");
        while (matcher.find(from)) {
            from = matcher.start() + 1;
            String templateVersion = matcher.group(1);
            templateVersions.add(templateVersion);
        }

        for (String templateVersion : templateVersions) {
            StringBuilder buildGradle = new StringBuilder();
            try {
                url = new URL("https://gitlab.com/dcostap/dareEngine-template/raw/" + templateVersion + "/build.gradle");
                is = url.openStream();
                br = new BufferedReader(new InputStreamReader(is));

                while ((line = br.readLine()) != null) {
                    buildGradle.append(line);
                }
            } catch (Exception e) {
                loading.close();

                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("Connection error");
                alert.setContentText("There was an error retrieving the information from the gitlab page of the template project." +
                        "\nCheck your internet connection and try again");
                alert.show();
                return;
            }

            Matcher engineVersionMatcher = Pattern.compile("(engineVersion = [\"'](.+?(?=[\"'])))|(compile 'com.gitlab.dcostap.template-project:core:(.+?(?=')))").matcher(buildGradle);

            if (!engineVersionMatcher.find()) continue;
            String engineVersion = engineVersionMatcher.group(2);
            if (engineVersionMatcher.group(2) == null) engineVersion = engineVersionMatcher.group(4);

            String pre = "";
            if (templateVersion.equals("master")) pre = "(latest) ";
            versions.add(pre + "template: '" + templateVersion + "';   engine: '" + engineVersion + "'");
        }

//        System.out.println(versions);

        versionsList.getItems().clear();
        versionsList.getItems().addAll(versions);
        if (versions.size() > 0) versionsList.setValue(versions.get(0));

        loading.close();
    }

    private void deleteDirectoryRecursively(String path) throws IOException {
        Path directory = Paths.get(path);
        if (!Files.exists(directory)) return;
        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                file.toFile().setWritable(true); // remove read-only
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public static void replacePackageNameInProject(String path, String originalPackages, String objectivePackageName) throws IOException {
        Path directory = Paths.get(path);
        if (!Files.exists(directory)) return;

        if (!Files.exists(Paths.get(directory + File.separator + "build.gradle"))) {
            throw new RuntimeException(path + " is not a libgdx project root folder");
        }

        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                String name = file.toFile().getName();
                if (name.endsWith(".png") || name.endsWith(".atlas") || name.endsWith(".hiero") || name.endsWith(".fnt")
                        || name.endsWith(".ttf") || name.endsWith(".json")) {
                    return FileVisitResult.CONTINUE;
                }

                // replace occurrences in a file
                file.toFile().setWritable(true);

                String content = new String(Files.readAllBytes(file), Charset.defaultCharset());

                content = content.replaceAll(originalPackages, objectivePackageName);
                Files.write(file, content.getBytes(Charset.defaultCharset()));

                System.out.println("Replaced stuff inside file: " + file);

                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                // rename a folder if it is one fo the package words
                File folder = dir.toFile();

                Matcher matcher = getMatcherPackageName(objectivePackageName);
                Matcher matcherOrig = getMatcherPackageName(originalPackages);
                matcher.find();
                matcherOrig.find();

                int i = 1;
                while (i <= matcher.groupCount() && i <= 3) {
                    if (folder.getName().equals(matcherOrig.group(i))) {
                        // apparently you need to do this to rename a folder; create a new one first
                        File newDir = new File(folder.getParent() + File.separator + matcher.group(i));
                        folder.renameTo(newDir);
                        System.out.println("new folder: " + newDir.toString());
                        break;
                    }

                    i++;
                }

                return FileVisitResult.CONTINUE;
            }
        });
    }
}
